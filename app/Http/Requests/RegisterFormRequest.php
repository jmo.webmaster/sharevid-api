<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required_with:name,email|confirmed',
            'email' => 'required_with:name|email|unique:users',
            'name' => 'required|string'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Digite seu nome',
            'email.required_with' => 'Digite seu email',
            'email.email' => 'Digite um endereço de email válido',
            'email.unique' => 'Já existe um cadastro com o email informado',
            'email.confirmed' => 'A confirmação da senha está incorreta',
        ];
    }
}
