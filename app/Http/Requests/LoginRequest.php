<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required',
            'email' => 'required|email',
        ];
    }

    public function messages(){
        return [
          'email.required' => 'Digite seu endereço de email',
          'email.email' => 'Digite um endereço de email válido',
          'password.required' => 'Digite sua senha',
        ];
    }
}
