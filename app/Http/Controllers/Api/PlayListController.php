<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Playlist;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class PlayListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        return $user->playlists()->with('videos')->orderBy('created_at', 'desc')->paginate('5');
    }

    public function select()
    {
        $user = auth()->user();
        return $user->playlists()->orderBy('created_at', 'desc')->get();
    }

    public function verificarUri($uri, $id = null){
        $play = Playlist::where(function ($query) use($uri, $id){
            $query->where('uri', $uri);
            if($id){
                $query->where('id', '!=', $id);
            }
        })->first();
        if($play){
            $uri = $uri.time();
        }
        return $uri;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $uri = Str::slug($request->name);
        $saved = $user->playlists()->create([
            'name' => $request->name,
            'uri' => $this->verificarUri($uri)
        ]);

        if($saved){
            return response()->json(['status' => true, 'msg' => 'Playlist cadastrada com sucesso!'], Response::HTTP_OK);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Playlist::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $play = Playlist::find($id);
        $uri = Str::slug($request->name);
        if($play){
            $play->name = $request->name;
            $play->uri = $this->verificarUri($uri, $id);
            $salved = $play->save();
            if($salved) {
                return response()->json(['status' => true, 'msg' => 'Playlist atualizada com sucesso!'], Response::HTTP_OK);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $play = Playlist::find($id);
        if($play){
            $deleted = $play->delete();
            if($deleted){
                return response()->json(['status' => true, 'msg' => 'Playlist excluída com sucesso!'], Response::HTTP_OK);
            }
        }
    }
}
