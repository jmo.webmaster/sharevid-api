<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(LoginRequest $request){
        if(!auth()->attempt($request->only('email', 'password'))){
            return response()->json(['msg' => 'Email ou senha incorretos'], Response::HTTP_NOT_FOUND);
        }

        $token = auth()->user()->createToken('user-token');

        return [
            'message' => ['Logado com sucesso'],
            'token' => $token->plainTextToken
        ];
    }

    public function user(){
        return auth()->user();
    }

    public function logout() {
        auth()->user()->currentAccessToken()->delete();
        return response()->json(['msg'=>'Successfully Logged out'], Response::HTTP_OK);
    }
}
