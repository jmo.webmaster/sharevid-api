<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{
    public function profile($id = null){
        if ($id){
            $getUser = User::with('videos')->find($id);
        }else{
            $getUser = auth()->user();
        }
        
        $dados = [];
        $dados['user'] = $getUser;
        if($getUser){
            $videos = $getUser->videos()->with('category')->paginate('20');
            $videos->append(['image', 'video']);
            if ($videos) {
                foreach ($videos as $key => $v){
                    $dados['videos'][$key] = $v;
                }
            }
        }
        return $dados;
    }

    public function videos(Request $request){
        $user = auth()->user();

        $getVideos = $user->videos()->with('category', 'playlist')->orderBy('created_at', 'desc')->paginate(12);
        $videos = [];
        
        
        $total = $getVideos->lastPage();
        $current_page = $getVideos->currentPage();
        if($getVideos->first()){
            foreach($getVideos as $v){
                $playlist = $v->playlist()->first();
                array_push($videos, [
                    'id' => $v->id,
                    'name' => $v->name,
                    'category' => ($v->category ? ['value' => $v->category->id, 'text' => $v->category->name] : []),
                    'path' => $v->video,
                    'cover' => $v->image,
                    'playlist' => ($playlist ? ['value' => $playlist->id, 'text' => $playlist->name] : [])
                ]);
            }
        }

        return response()->json(['videos' => $videos, 'total' => $total, 'current_page' => $current_page], Response::HTTP_OK);
    }

    public function video($user, $uri){
        $user = User::where('uri', $user)->first();
        $dados = [];
        $dados['user'] = $user;
        if($user){
            $video = $user->videos()->where('uri', $uri)->first();
            $video->append('video', 'image');
            $dados['video'] = $video;
        }

        return $dados;
    }

    public function following(){

    }
}
