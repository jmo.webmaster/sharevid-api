<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $uri = $this->verificarUri(Str::slug($request->name));
        $salvo = $user->videos()->create([
            'name' => $request->name,
            'category_id' => $request->category,
            'uri' => $uri
        ]);

        if($salvo){
            $video = $request->file('video');
            if(!empty($video)){
                $this->uploadVideo($video, $uri, $user, $salvo);
            }

            $cover = $request->file('cover');
            if(!empty($cover)){
                $this->uploadCover($cover, $uri, $user, $salvo);
            }

            if ($request->playlist){
                $salvo->playlist()->sync([$request->playlist]);
            }
        }

        return response()->json(['status' => true, 'msg' => 'Vídeo(s) enviado(s) com sucesso!'], Response::HTTP_OK);
    }

    public function uploadVideo($video, $uri, $user, $bd){
        $extensao = '.'.$video->getClientOriginalExtension();
        $nome = 'videos/'.$uri;
        $upload = $video->storeAs('users',  $user->uuid.'/'.$nome.$extensao, 'public');
        if($upload){
            $bd->path = $nome.$extensao;
            $bd->save();
        }
    }

    public function uploadCover($cover, $uri, $user, $bd){
        $extensao = '.'.$cover->getClientOriginalExtension();
        $nome = 'covers/'.$uri;
        $upload = $cover->storeAs('users',  $user->uuid.'/'.$nome.$extensao, 'public');
        if($upload){
            $bd->cover = $nome.$extensao;
            $bd->save();
        }
    }

    public function verificarUri($uri, $id = null){
        $video = Video::where(function ($query) use($uri, $id){
            $query->where('uri', $uri);
            if($id){
                $query->where('id', '!=', $id);
            }
        })->first();
        if($video){
            $uri = $uri.time();
        }
        return $uri;
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $getVideo = Video::find($id);
        if($getVideo){
            $uri = $this->verificarUri(Str::slug($request->name), $id);
            $getVideo->name = $request->name;
            $getVideo->uri = $uri;
            $getVideo->category_id = $request->category;
            $salvo = $getVideo->save();

            if($salvo){
                $video = $request->file('video');
                if(!empty($video)){
                    $path = 'users/'.$getVideo->user->uuid.'/'.$getVideo->path;
                    Storage::delete($path);
                    $this->uploadVideo($video, $uri, $user, $getVideo);
                }

                $cover = $request->file('cover');
                if(!empty($cover)){
                    $path = 'users/'.$getVideo->user->uuid.'/'.$getVideo->cover;
                    Storage::delete($path);
                    $this->uploadCover($cover, $uri, $user, $getVideo);
                }

                $getVideo->playlist()->sync([$request->playlist]);
            }
        }
        return response()->json(['status' => true, 'msg' => 'Vídeo(s) atualizado(s) com sucesso!'], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::with('user')->find($id);
        if($video){
            $path = 'users/'.$video->user->uuid.'/'.$video->path;
            if (Storage::delete($path)) {
                $excluido = $video->delete();
            } else {
                $excluido = $video->delete();
            }
            if($excluido){
                Storage::delete('users/'.$video->user->uuid.'/'.$video->cover);
                return response()->json(['status' => true], Response::HTTP_OK);
            }
        }
    }
}
