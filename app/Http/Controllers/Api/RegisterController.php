<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterFormRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    public function register(RegisterFormRequest $request){
        $registered = User::create([
            'name' => $request->name,
            'uri' => Str::slug($request->name).'-'.time(),
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if (!$registered){
            return response()->json(['status' => true, 'msg' => 'Ouve um erro ao tentar cadastrar seu usuário, tente novamente!'], Response::HTTP_NOT_FOUND);
        }

        return response()->json(['msg' => 'Cadastro realizdo com sucesso!'], Response::HTTP_OK);
    }
}
