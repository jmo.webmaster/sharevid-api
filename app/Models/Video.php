<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Video extends Model
{
    use HasFactory;

    protected $table = 'videos';
    protected $fillable = [
        'user_id', 'cover', 'views', 'path', 'name', 'uri', 'category_id'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function category(){
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function playlist(){
        return $this->belongsToMany(Playlist::class, 'videos_playlist');
    }

    public function getVideoAttribute(){
        $path = 'users/'.$this->user->uuid.'/'.$this->path;
        return (!empty($this->path) ? (Storage::disk('public')->exists($path) ? Storage::url($path) : '') : '');
    }

    public function getImageAttribute(){
        $path = 'users/'.$this->user->uuid.'/'.$this->cover;
        return (!empty($this->cover) ? (Storage::disk('public')->exists($path) ? Storage::url($path) : '') : '');
    }
}
