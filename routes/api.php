<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\PlayListController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\VideoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [RegisterController::class, 'register']);
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', [AuthController::class, 'user']);
    Route::get('/logout', [AuthController::class, 'logout']);

    Route::resource('/categories', CategoryController::class);
    Route::resource('videos', VideoController::class);
    
    //Profile controller
    Route::get('profile/videos', [ProfileController::class, 'videos']);
    Route::get('profile/{id?}', [ProfileController::class, 'profile']);
    Route::get('profile/{user}/{uri}', [ProfileController::class, 'video']);

    //Playlists
    Route::get('playlists/select', [PlayListController::class, 'select']);
    Route::resource('playlists', PlayListController::class);
});
